package com.ldig.beecare;

import com.google.firebase.firestore.GeoPoint;

public class Child {

    private String name;
    private String currentLoc;
    private String phoneNumber;
    private GeoPoint geopoint;

    public GeoPoint getGeopoint() {
        return geopoint;
    }

    public void setGeopoint(GeoPoint geopoint) {
        this.geopoint = geopoint;
    }

    public Child(){

    }
    public Child(String name, String currentLoc, String phoneNumber){
        this.name = name;
        this.currentLoc = currentLoc;
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCurrentLoc() {
        return currentLoc;
    }

    public void setCurrentLoc(String currentLoc) {
        this.currentLoc = currentLoc;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
