package com.ldig.beecare;

import com.google.firebase.firestore.GeoPoint;

public class TrustedPlace {
    private String name;
    private GeoPoint geopoint;

    public TrustedPlace(){

    }

    public TrustedPlace(String name, GeoPoint geopoint){
        this.name = name;
        this.geopoint = geopoint;
    }

    public String getName() {
        return name;
    }

    public GeoPoint getGeopoint() {
        return geopoint;
    }

    public void setGeopoint(GeoPoint geopoint) {
        this.geopoint = geopoint;
    }

    public void setName(String name) {
        this.name = name;
    }
}
