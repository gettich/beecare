package com.ldig.beecare;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.hardware.TriggerEvent;
import android.hardware.TriggerEventListener;
import android.os.Bundle;
import android.provider.Telephony;
import android.telephony.CellIdentityLte;
import android.telephony.CellInfo;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainActivity extends AppCompatActivity {

    private EditText loginText;
    private EditText passText;
    private Switch parentSwitch;
    private Button signInButton;

    private FirebaseAuth mAuth;
    private SensorManager sensorManager;
    private Sensor sensor;
    private TriggerEventListener triggerHandler;
    private TelephonyManager tm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //ExecutorService executorService = Executors.newSingleThreadExecutor();
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_SIGNIFICANT_MOTION);
        tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        triggerHandler = new TriggerEventListener() {
            @Override
            public void onTrigger(TriggerEvent event) {
                Log.i("fdsfs", "Fdsf");
                sensorManager.requestTriggerSensor(this, sensor);
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                List<CellInfo> cellInfoList = tm.getAllCellInfo();
                CellIdentityLte lte = (CellIdentityLte)cellInfoList.get(0).getCellIdentity();

                Log.i("cellid", String.valueOf(lte.getCi()));
                Log.i("cellid", String.valueOf(lte.getTac()));
                Log.i("cellid", lte.getMccString());
                Log.i("cellid", lte.getMncString());
            }
        };
        sensorManager.requestTriggerSensor(triggerHandler, sensor);
        loginText = findViewById(R.id.loginBox);
        passText = findViewById(R.id.passwordText);
        parentSwitch = findViewById(R.id.isParentAccountSwitch);
        signInButton = findViewById(R.id.signInButton);

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn(loginText.getText().toString(), passText.getText().toString());
            }
        });

        parentSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Intent intent = new Intent(MainActivity.this, generateCodeActivity.class);
                startActivity(intent);
                parentSwitch.setChecked(true);
            }
        });
        mAuth = FirebaseAuth.getInstance();

    }

    @Override
    public void onStart()
    {
        super.onStart();
        FirebaseUser user = mAuth.getCurrentUser();
        if (user != null)
        {
        }
    }

    private void signIn(String login, String password)
    {
        mAuth.signInWithEmailAndPassword(login, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful())
                        {
                            FirebaseUser user = mAuth.getCurrentUser();
                            Intent intent = new Intent(MainActivity.this, parentActivity.class);
                            startActivity(intent);
                        }
                        else
                        {

                        }
                    }
                });
    }

    public void onSignUpClick(View view)
    {
        Intent intent = new Intent(this, signUpActivity.class);
        startActivity(intent);
    }
}
