package com.ldig.beecare;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.functions.FirebaseFunctions;
import com.google.firebase.functions.HttpsCallableResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class addChildActivity extends AppCompatActivity {

    private FirebaseFunctions mFunctions;
    private Button addButton;
    private EditText securityCode;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_child);
        mFunctions = FirebaseFunctions.getInstance();
        addButton = findViewById(R.id.addChildButton);
        securityCode = findViewById(R.id.securityCodeBox);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkCode(securityCode.getText().toString()).addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        JSONObject result = null;
                        try {
                            result = new JSONObject((String)task.getResult());
                            if(result.getBoolean("success"))
                                finish();

                        } catch (JSONException e) {
                        }
                    }
                });
            }
        });
    }
    private Task<String> checkCode(String code)
    {
        Map<String, Object> data = new HashMap<>();
        data.put("code", code);
        return mFunctions.getHttpsCallable("checkCode")
                .call(data)
                .continueWith(new Continuation<HttpsCallableResult, String>() {
                    @Override
                    public String then(@NonNull Task<HttpsCallableResult> task) throws Exception {
                        String result = (String) task.getResult().getData().toString();
                        return result;
                    }
                });
    }
}