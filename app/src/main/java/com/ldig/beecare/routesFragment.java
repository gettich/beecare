package com.ldig.beecare;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class routesFragment extends Fragment {

    private List<TrustedPlace> trustedPlaces;
    private ListView placesListView;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.routes_fragment, container, false);
        placesListView = view.findViewById(R.id.routesList);
        getTrustedPlaces();
        FloatingActionButton fab = view.findViewById(R.id.addRoute);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               getParentFragmentManager().beginTransaction().replace(R.id.fragment_container, new addRouteFragment()).addToBackStack(null).commit();
            }
        });
        return view;
    }
    private List<TrustedPlace> getTrustedPlaces()
    {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        FirebaseFirestore ff = FirebaseFirestore.getInstance();
        trustedPlaces = new ArrayList<>();
        ff.collection("users").document(firebaseUser.getUid()).collection("trustedPlaces").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            List<DocumentSnapshot> myListOfDocuments = task.getResult().getDocuments();
                            for (DocumentSnapshot dc : myListOfDocuments) {
                                TrustedPlace p = dc.toObject(TrustedPlace.class);
                                //Log.i("test", child.getPhoneNumber());
                                trustedPlaces.add(p);
                            }
                        }
                        TrustedPlacesAdapter tpAdapter = new TrustedPlacesAdapter(getActivity(), trustedPlaces);
                        placesListView.setAdapter(tpAdapter);

                    }
                });
        return trustedPlaces;
    }
}
