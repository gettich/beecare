package com.ldig.beecare;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import java.util.List;

public class TrustedPlacesAdapter extends ArrayAdapter<TrustedPlace> {
    public TrustedPlacesAdapter(Context context, List<TrustedPlace> places) {
        super(context, 0, places);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        TrustedPlace tp = getItem(position);
        if(convertView == null)
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listview_trusted_places,parent, false);
        TextView placeText = convertView.findViewById(R.id.placeName);
        TextView locationText = convertView.findViewById(R.id.coords);
        placeText.setText(tp.getName());
        String lat = String.valueOf(tp.getGeopoint().getLatitude());
        String lng = String.valueOf(tp.getGeopoint().getLongitude());
        locationText.setText(lat + ";" + lat);

        return convertView;
    }
}
