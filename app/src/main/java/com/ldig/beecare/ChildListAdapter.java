package com.ldig.beecare;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

public class ChildListAdapter extends ArrayAdapter<Child> {

    public ChildListAdapter(Context context, List<Child> children) {
        super(context, 0, children);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        Child child = getItem(position);
        if(convertView == null)
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listview_child_layout,parent, false);
        TextView childText = convertView.findViewById(R.id.childName);
        TextView locationText = convertView.findViewById(R.id.location);
        ImageButton callButton = convertView.findViewById(R.id.callButton);
        childText.setText(child.getName());
        locationText.setText(child.getCurrentLoc());
        callButton.setOnClickListener(v -> {
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:" + child.getPhoneNumber()));
            ContextCompat.startActivity(getContext(), callIntent, null);
        });

        return convertView;
    }
}
