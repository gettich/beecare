package com.ldig.beecare;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.functions.FirebaseFunctions;
import com.google.firebase.functions.HttpsCallableResult;

import org.json.JSONException;
import org.json.JSONObject;

public class generateCodeActivity extends AppCompatActivity {

    private FirebaseFunctions mFunctions;
    private TextView code;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generate_code);
        mFunctions = FirebaseFunctions.getInstance();
        GenerateCode().addOnCompleteListener(new OnCompleteListener<String>() {
            @Override
            public void onComplete(@NonNull Task<String> task) {
                JSONObject result = null;
                try {
                    result = new JSONObject((String)task.getResult());
                } catch (JSONException e) {
                    finish();
                }
                String codestr = null;
                try {
                    codestr = result.getString("generated");
                } catch (JSONException e) {
                    finish();
                }
                code = findViewById(R.id.code);
                code.setText(codestr);
            }
        });

    }

    private Task<String> GenerateCode()
    {
        return mFunctions.getHttpsCallable("generateCode")
                .call()
                .continueWith(new Continuation<HttpsCallableResult, String>() {
                    @Override
                    public String then(@NonNull Task<HttpsCallableResult> task) throws Exception {
                        String result = (String) task.getResult().getData().toString();
                        return result;
                    }
                });
    }

}